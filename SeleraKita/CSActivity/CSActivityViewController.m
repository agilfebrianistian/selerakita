//
//  CSActivityViewControllerViewController.m
//  SEAKR
//
//  Created by Agil Febrianistian on 3/11/13.
//  Copyright (c) 2013 Agil Febrianistian. All rights reserved.
//

#import "CSActivityViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface CSActivityViewController ()

@end

@implementation CSActivityViewController
@synthesize activityContainer;
@synthesize activityIndicator;


static  CSActivityViewController *sharedObject=nil;


+(CSActivityViewController *)sharedObject{
    @synchronized(self)
    {
        if (sharedObject == nil)
        {
            UIStoryboard *myStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            sharedObject = (CSActivityViewController *)[myStoryboard instantiateViewControllerWithIdentifier:@"csactivity"];
        }
    }
    return sharedObject;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithHue:0.0 saturation:0.0 brightness:0.0 alpha:0.5]];
    self.activityContainer.layer.cornerRadius = 10;
    
    self.view.layer.position =CGPointMake([[UIScreen mainScreen]bounds].size.width/2, [[UIScreen mainScreen]bounds].size.height/2);
}

- (void)viewDidUnload
{
    [self setActivityContainer:nil];
    [self setActivityIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)showActivityView
{
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    [window addSubview:self.view];
    NSLog(@"%f",[[UIScreen mainScreen]bounds].size.width);
    NSLog(@"%f",[[UIScreen mainScreen]bounds].size.height);
    
}

-(void)hideActivityView
{
    [self.view removeFromSuperview];
}

@end
