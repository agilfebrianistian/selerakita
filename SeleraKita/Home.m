//
//  ViewController.m
//  SeleraKita
//
//  Created by Agil Febrianistian on 2/9/15.
//  Copyright (c) 2015 KuyaInside. All rights reserved.
//

#import "Home.h"
#import "UIImageView+WebCache.h"
#import "NFXIntroViewController.h"

#define devHeight  self.view.bounds.size.height
#define devWidth   self.view.bounds.size.width


@interface Home ()

@end

@implementation Home
@synthesize topData,recommendationData,recommendationTable;
@synthesize carouselView,homeSearch;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    UIImage*i1 = [UIImage imageNamed:@"1"];
//    UIImage*i2 = [UIImage imageNamed:@"2"];
//    UIImage*i3 = [UIImage imageNamed:@"3"];
//    UIImage*i4 = [UIImage imageNamed:@"4"];
//    
//    
//    NFXIntroViewController*vc = [[NFXIntroViewController alloc] initWithViews:@[i1,i2,i3,i4]];
//    [self presentViewController:vc animated:true completion:nil];
    
    
    [self addNavigationItem];
    
    [self pullableSet];
    
    [homeSearch setBackgroundImage:[UIImage imageWithCGImage:(__bridge CGImageRef)([UIColor clearColor])]];
    
    topData             = [[NSMutableArray alloc]init];
    recommendationData  = [[NSMutableArray alloc]init];
    
    carouselView = [carouselView initWithFrame:CGRectMake(29, 500, devWidth - (devWidth/8) ,  devHeight - (devHeight/3))];
    
   
    carouselView.type=iCarouselTypeCoverFlow;
    
    longitude = 0;
    latitude  = 0;
    
    [self locationCheck];
    
    self.definesPresentationContext = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}



-(void)pullableSet
{
    CGFloat xOffset = 0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        xOffset = 224;
    }

    
    pullUpView = [[StyledPullableView alloc] initWithFrame:CGRectMake(xOffset, 0, 320, 460)];
    pullUpView.openedCenter = CGPointMake(160 + xOffset,self.view.frame.size.height);
    pullUpView.closedCenter = CGPointMake(160 + xOffset, self.view.frame.size.height + 200);
    pullUpView.center = pullUpView.closedCenter;
    pullUpView.handleView.frame = CGRectMake(0, 0, 320, 40);
    pullUpView.delegate = self;
    pullUpView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:pullUpView];
    
    pullUpLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 4, 320, 20)];
    pullUpLabel.textAlignment = NSTextAlignmentCenter;
    pullUpLabel.backgroundColor = [UIColor clearColor];
    pullUpLabel.textColor = [UIColor lightGrayColor];
    pullUpLabel.text = @"recommendation";
    
    [pullUpView addSubview:pullUpLabel];

    recommendationTable.frame = CGRectMake(0,30,320,200);
    
    [pullUpView addSubview:recommendationTable];
    
}

#pragma mark -
#pragma mark Add Navigation Item

- (void)addNavigationItem
{
    CGRect frame = CGRectMake(0, 0, 0, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame] ;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"selerakita.info";
    self.navigationItem.titleView = label;
    

    UIBarButtonItem *rightNotificationItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshAllData)];
    
    self.navigationItem.rightBarButtonItem = rightNotificationItem;
    
}

- (void)refreshAllData
{
  [[CSActivityViewController sharedObject] showActivityView];
  [self updatedataWith:longitude and:latitude];
}
#pragma mark - LOCATION

-(void)locationCheck
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if(IS_OS_8_OR_LATER){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [locationManager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [locationManager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
   

    [locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [locationManager stopUpdatingLocation];
    
    latitude = locationManager.location.coordinate.latitude;
    longitude = locationManager.location.coordinate.longitude;
    
    [[CSActivityViewController sharedObject] showActivityView];
    
    [self updatedataWith:longitude and:latitude];
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
  UIAlertView *errorAlert = [[UIAlertView alloc]
                              initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
  [errorAlert show];
    
  [[CSActivityViewController sharedObject] showActivityView];
  [self updatedataWith:0 and:0];
}


#pragma mark -
#pragma mark get data from server

- (void)updatedataWith:(float)alongitude and:(float)alatitude
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params = @{@"location[latitude]": [NSString stringWithFormat:@"%f",alatitude],
                             @"location[longitude]": [NSString stringWithFormat:@"%f",alongitude],
                             @"page" : @"1"
                             };
    
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:[NSString stringWithFormat:@"%@",SERVICE_URL]   parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [[CSActivityViewController sharedObject]hideActivityView];
        
     //   NSLog(@"responseObject = %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        
        NSLog(@"%@", responseObject);
        
        

        topData = [[NSMutableArray alloc]init];
        
        
        NSMutableArray* recommendedRestourant = [[[(NSMutableArray*)responseObject valueForKey:@"restaurants"] valueForKey:@"recommendations"] mutableCopy];
        
        for (NSMutableArray*items in recommendedRestourant) {
            NSMutableArray* tempitem = [items mutableCopy];
            [recommendationData addObject:tempitem];
        }
        
        
        NSMutableArray* topRestaurant = [[[(NSMutableArray*)responseObject valueForKey:@"restaurants"] valueForKey:@"top_restaurants"] mutableCopy];
        for (NSMutableArray*items in topRestaurant) {
            NSMutableArray* tempitem = [items mutableCopy];
            [topData addObject:tempitem];
        }
        
        [carouselView reloadData];
        [recommendationTable reloadData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [[CSActivityViewController sharedObject]hideActivityView];
        UIAlertView* message  = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Connection Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [message show];
        
    }];
}

#pragma mark -
#pragma mark iCarousel

-(NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return topData.count;
}

- (UIView *) carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    
    UIImageView * timelineImages = Nil;
    UIImageView * timelinethumb1 = Nil;
    UIImageView * timelinethumb2 = Nil;
    UIImageView * timelinethumb3 = Nil;
    UIImageView * timelinethumb4 = Nil;
    UILabel * timelineTitle = Nil;
    UILabel * timelineAddress = Nil;
    
//    CGRectMake(0, 0, devWidth - (devWidth/8) , devHeight/3 )
    
    if (view == nil)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, devWidth - (devWidth/8) ,  devHeight - (devHeight/3))];
        view.contentMode = UIViewContentModeCenter;
        view.backgroundColor = [UIColor whiteColor];
        view.layer.cornerRadius = 5.0f;
        view.clipsToBounds = YES;
        
        timelineImages = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, devWidth - (devWidth/8), 195.0f)];
        timelineImages.tag = 1;
        [view addSubview:timelineImages];
        
        timelineTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 270, 300.0f, 20.0f)];
        timelineTitle.tag = 2;
        timelineTitle.font = [UIFont fontWithName:@"Helvetica Light" size:17.0f];
        
        [view addSubview:timelineTitle];

        timelineAddress = [[UILabel alloc]initWithFrame:CGRectMake(20, 280, 300.0f, 40.0f)];
        timelineAddress.tag = 3;
        timelineAddress.font = [UIFont fontWithName:@"Helvetica Light" size:14.0f];
        
        [view addSubview:timelineAddress];
        
        float yOffset = 200.0f;
        float xyImage = 63.0f;
        
        
        timelinethumb1 = [[UIImageView alloc]initWithFrame:CGRectMake(((xyImage+4)* 0), yOffset, xyImage, xyImage)];
        timelinethumb1.tag = 11;
        timelinethumb1.clipsToBounds=YES;
        timelinethumb1.contentMode = UIViewContentModeScaleAspectFill;
        [view addSubview:timelinethumb1];
        
        timelinethumb2 = [[UIImageView alloc]initWithFrame:CGRectMake(((xyImage+4)* 1), yOffset, xyImage, xyImage)];
        timelinethumb2.tag = 12;
        timelinethumb2.clipsToBounds=YES;
        timelinethumb2.contentMode = UIViewContentModeScaleAspectFill;
        [view addSubview:timelinethumb2];
        
        timelinethumb3 = [[UIImageView alloc]initWithFrame:CGRectMake(((xyImage+4)* 2), yOffset, xyImage, xyImage)];
        timelinethumb3.tag = 13;
        timelinethumb3.clipsToBounds=YES;
        timelinethumb3.contentMode = UIViewContentModeScaleAspectFill;
        [view addSubview:timelinethumb3];
        
        timelinethumb4 = [[UIImageView alloc]initWithFrame:CGRectMake(((xyImage+4)* 3), yOffset, xyImage, xyImage)];
        timelinethumb4.tag = 14;
        timelinethumb4.clipsToBounds=YES;
        timelinethumb4.contentMode = UIViewContentModeScaleAspectFill;
        [view addSubview:timelinethumb4];

    }
    else
    {
        timelineImages = (UIImageView *)[view viewWithTag:1];
        timelineTitle = (UILabel *)[view viewWithTag:2];
        timelineAddress = (UILabel *)[view viewWithTag:3];
        
        timelinethumb1 = (UIImageView *)[view viewWithTag:11];
        timelinethumb2 = (UIImageView *)[view viewWithTag:12];
        timelinethumb3 = (UIImageView *)[view viewWithTag:13];
        timelinethumb4 = (UIImageView *)[view viewWithTag:14];

    }

    NSMutableArray* items =[topData objectAtIndex:index];
    
   [timelineImages sd_setImageWithURL:[NSURL URLWithString:[items valueForKey:@"default_image"]] placeholderImage:nil options:SDWebImageRefreshCached];
    
    NSMutableDictionary * images = [items valueForKey:@"thumb_images"];
    
    int counter = 0;
    
    for (NSDictionary *image in images) {
    
        switch (counter) {
            case 0:
                [timelinethumb1 sd_setImageWithURL:[NSURL URLWithString:[image valueForKey:@"url"]] placeholderImage:nil options:SDWebImageRefreshCached];
                break;
            case 1:
                [timelinethumb2 sd_setImageWithURL:[NSURL URLWithString:[image valueForKey:@"url"]] placeholderImage:nil options:SDWebImageRefreshCached];
                break;
            case 2:
                [timelinethumb3 sd_setImageWithURL:[NSURL URLWithString:[image valueForKey:@"url"]] placeholderImage:nil options:SDWebImageRefreshCached];
                break;
            case 3:
                [timelinethumb4 sd_setImageWithURL:[NSURL URLWithString:[image valueForKey:@"url"]] placeholderImage:nil options:SDWebImageRefreshCached];
                break;
            default:
                break;
        }
    
        counter++;
    }
    
    timelineTitle.text = [items valueForKey:@"name"];
    timelineAddress.text = [items valueForKey:@"street"];
    
    return view;
}

- (BOOL)carousel:(iCarousel *)carousel shouldSelectItemAtIndex:(NSInteger)index{
    
    return YES;
}



#pragma mark - Search Bar Delegate

- (void)searchBarSearchButtonClicked:(UISearchBar*)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar;
{
    
    [searchBar resignFirstResponder];
    
}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    searchBar.showsCancelButton = YES;
    
    [searchBar setFrame:CGRectMake(0, 20, 320 - 80, 44)];

//    [UIView animateWithDuration:.4
//                     animations:^{
//                         homeSearch.frame = CGRectMake(searchBar.frame.origin.x,
//                                                      0,
//                                                      searchBar.frame.size.width,
//                                                      searchBar.frame.size.height);
//                     }
//                     completion:^(BOOL finished){
//                         //whatever else you may need to do
//                     }];

}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
    
   [searchBar setFrame:CGRectMake(0, 64, 320 - 80, 44)];
    searchBar.showsCancelButton = NO;
//    [UIView animateWithDuration:.4
//                     animations:^{
//                         searchBar.frame = CGRectMake(searchBar.frame.origin.x,
//                                                      0,
//                                                      searchBar.frame.size.width,
//                                                      searchBar.frame.size.height);
//                     }
//                     completion:^(BOOL finished){
//                         //whatever else you may need to do
//                     }];

}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [recommendationData count] ? [recommendationData count] : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary* item = [recommendationData objectAtIndex:indexPath.row];
    
    NSString *cellIdentifier = @"RecommendationCell";
    RecommendationCell *cell =  (RecommendationCell*) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[RecommendationCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }

    
    [cell.imageRecommendation sd_setImageWithURL:[NSURL URLWithString:[item valueForKey:@"default_image"]] placeholderImage:nil options:SDWebImageRefreshCached];
    
    
    cell.titleRecommendation.text = [item valueForKey:@"name"];
    cell.addressRecommendation.text = [item valueForKey:@"street"];

    
    return cell;
    
    
}



@end
