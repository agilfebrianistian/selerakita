//
//  AppDelegate.h
//  SeleraKita
//
//  Created by Agil Febrianistian on 2/9/15.
//  Copyright (c) 2015 KuyaInside. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
