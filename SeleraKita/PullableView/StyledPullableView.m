
#import "StyledPullableView.h"

/**
 @author Fabio Rodella fabio@crocodella.com.br
 */

@implementation StyledPullableView

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Legend-FrameOption"]];
        imgView.frame = CGRectMake(0, 0, 190, 220);
        [self addSubview:imgView];
    }
    return self;
}

@end
