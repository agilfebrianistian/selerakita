//
//  RecomendationCell.m
//  SeleraKita
//
//  Created by Agil Febrianistian on 3/11/15.
//  Copyright (c) 2015 KuyaInside. All rights reserved.
//

#import "RecommendationCell.h"

@implementation RecommendationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
