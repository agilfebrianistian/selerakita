//
//  RecomendationCell.h
//  SeleraKita
//
//  Created by Agil Febrianistian on 3/11/15.
//  Copyright (c) 2015 KuyaInside. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecommendationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageRecommendation;
@property (weak, nonatomic) IBOutlet UILabel *titleRecommendation;
@property (weak, nonatomic) IBOutlet UILabel *addressRecommendation;

@property (weak, nonatomic) IBOutlet UIButton *likeButtonRecommendation;
@property (weak, nonatomic) IBOutlet UIButton *commentButtonRecommendation;


@end
