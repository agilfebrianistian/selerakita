//
//  ViewController.h
//  SeleraKita
//
//  Created by Agil Febrianistian on 2/9/15.
//  Copyright (c) 2015 KuyaInside. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "AFNetworking.h"
#import "CSActivityViewController.h"
#import "Constant.h"
#import <CoreLocation/CoreLocation.h>
#import "StyledPullableView.h"
#import "RecommendationCell.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface Home : UIViewController<iCarouselDataSource,iCarouselDelegate,CLLocationManagerDelegate,PullableViewDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate, UISearchDisplayDelegate>
{
    CLLocationManager *locationManager;
    StyledPullableView *pullUpView;
    UILabel *pullUpLabel;
    float longitude;
    float latitude;
    
}

@property (strong, nonatomic) NSMutableArray* topData;
@property (strong, nonatomic) NSMutableArray* recommendationData;


@property (weak, nonatomic) IBOutlet UISearchBar *homeSearch;
@property (strong, nonatomic) IBOutlet iCarousel *carouselView;
@property (strong, nonatomic) IBOutlet UITableView *recommendationTable;





@end
